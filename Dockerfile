FROM python:3.8
ARG APP_VERSION=latest
ENV APP_VERSION=${APP_VERSION}

RUN groupadd user && useradd -g user user
WORKDIR /var/www

COPY ./requirements/*.txt ./
RUN pip install -r ./main.txt && \
    pip install -r ./dev.txt

COPY ./wait-for-it.sh /wait-for-it.sh
COPY . .
RUN chmod -R 550 /var/www && chown -R user:user /var/www

USER user
